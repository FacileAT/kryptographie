#!/usr/bin/python
#
# Übung 2, Beispiel 3: Stromcipher
#

# Konfigurationen
import sys, getopt
import numpy as np
import matplotlib.pyplot as plt
TAPPED = [32, 70, 82, 89, 91, 127]
SHIFTS = 10000
TIME = 0
IV = 0
KEY = 0
FILE = ""
regM = []
regKey = []
regIV = []
regC = bytearray()


def main(argv):
    if len(sys.argv) != 7:
        print ("Zu wenig Argumente angegeben!")
        exit()
    try:
        opts, args = getopt.getopt(argv,"k:i:f:", ["key=", "iv=", "ofile="])
    except getopt.GetoptError:
        print ("U2Stromcipher.py -k <key as HEX> -iv <IV as HEX> -f <outputfile>")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-k", "--key"):
            try:
                global KEY
                KEY = int('0x' + arg, 16)
            except ValueError:
                print("Falsche Eingabe: Key muss als HEX Wert übergeben werden!")
                sys.exit()
        elif opt in ("-i", "--iv"):
            try:
                global IV
                IV = int('0x' + arg, 16)
            except ValueError:
                print("Falsche Eingabe: IV muss als HEX Wert übergeben werden!")
                exit()
        elif opt in ("-f", "--ofile"):
            global FILE
            FILE = arg


def shiftKeyReg(x):
    # Register regKey x mal shiften und Output verwerfen
    for i in range(x):
        tmpBit0 = 0
        for t in TAPPED:
            tmpBit0 += int(regKey[t])
        
        regKey.insert(0, tmpBit0 % 2)
        regKey.pop()

def shiftKeyOutput(x):
    # Register regKey x mal shiften und Output verwerfen
    tmpRegOutput = ""
    for i in range(x):
        tmpBit0 = 0
        tmpRegOutput = str(regKey[len(regKey) - 1]) + tmpRegOutput
        for t in TAPPED:
            tmpBit0 += int(regKey[t])
   
        regKey.insert(0, tmpBit0 % 2)
        regKey.pop()
    return tmpRegOutput

def initKeyReg():
    # Key und IV Register initialisieren
    # Beide Register Bitweise 0 setzen
    for i in range(0, 128):
        regKey.append(0)

    for i in range(0, 64):
        regIV.append(0)

    # Key und IV ins Register laden
    # (durch das [2:] wird beim Binärwert erst der 2. Index berücksichtigt, somit fällt das 0b weg)
    tmpStart = 64 - len(bin(IV)) + 2
    for binDigit in bin(IV)[2:]:
        regIV[tmpStart] = binDigit
        tmpStart += 1

    tmpStart = 128 - len(bin(KEY)) + 2
    for binDigit in bin(KEY)[2:]:
        regKey[tmpStart] = binDigit
        tmpStart += 1

    # Key Register 128 mal shiften und Output verwerfen
    shiftKeyReg(128)

    # IV und Key XOR verknüpfen und das Register weitere 128 mal takten
    for i in range(0, 64):
        regKey[i] = int(regKey[i]) ^ int(regIV[i])
    
    shiftKeyReg(128)

def crypt():
    tmp=0
    for b in regM:
        regC.append(b ^ int(shiftKeyOutput(8), 2))

def readData():
    with open(FILE, "rb") as f:
        # gibt Byteobjekte zurück
        global regM
        regM = f.read()
    f.closed

def writeData():
    with open(FILE + ".enc", "wb") as f:
        f.write(regC)
    f.closed

def plot():
    #matrix = np.matrix(np.array([[int(regC[i+j]) for i in range(256)] for j in range(256)]))
    matrix = np.zeros((128, 128))
    for i in range(128):
        for j in range(128):
            matrix[i, j] = regC[(127 * j) + i]
    plt.matshow(matrix, cmap=plt.cm.gray)



print("")
print("Übung 2 - 128 Bit Schieberegister")
print("=================================")
print("")

if __name__ == "__main__":
    main(sys.argv[1:])

initKeyReg()
readData()
crypt()
writeData()
plot()
plt.show()

print("")
print("Die Datei", FILE, "wurde erfolgreich verschlüsselt.")
