# PHP Backend System

PHP based REST API


## 1. Prepare Database and Config Files


## 2. Database
All SQL Statements for creating the Database objects are in the folder scripts/sql.

### User
|Column    |Datatype    |Primary Key|Description                      |
|----------|------------|:---------:|---------------------------------|
|id        |INT(6)      |X          |                                 |
|first_name|VARCHAR(30) |           |                                 |
|last_name |VARCHAR(30) |           |                                 |
|login_name|VARCHAR(30) |           |The login_name have to be unique!|
|password  |VARCHAR(256)|           |SHA256 Password String           |
|status    |INT(1)      |           |0...User inactive<br>1...User active|

### User_Roles
### User_Log
### Roles
### Routes
