#!/usr/bin/python
#
# Übung 2, Beispiel 3: Stromcipher
#

# Konfigurationen
import sys, getopt
TAPPED = [32, 70, 82, 89, 91, 127]
SHIFTS = 1000
TIME = 0
IV = 0
KEY = 0
regM = []
regKey = []
regIV = []


def main(argv):
    if len(sys.argv) != 5:
        print ("Zu wenig Argumente angegeben!")
        print ("U2Stromcipher.py -k <key as HEX> -i <IV as HEX>")
        exit()
    try:
        opts, args = getopt.getopt(argv,"k:i:", ["key=", "iv="])
    except getopt.GetoptError:
        print ("U2cStromcipher.py -k <key as HEX> -i <IV as HEX>")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-k", "--key"):
            try:
                global KEY
                KEY = int('0x' + arg, 16)
            except ValueError:
                print("Falsche Eingabe: Key muss als HEX Wert übergeben werden!")
                sys.exit()
        elif opt in ("-i", "--iv"):
            try:
                global IV
                IV = int('0x' + arg, 16)
            except ValueError:
                print("Falsche Eingabe: IV muss als HEX Wert übergeben werden!")
                exit()


def shiftKeyReg(x):
    # Register regKey x mal shiften und Output verwerfen
    for i in range(x):
        tmpBit0 = 0
        for t in TAPPED:
            tmpBit0 += int(regKey[t])
        
        regKey.insert(0, tmpBit0 % 2)
        regKey.pop()

    print("LSFR", x, "mal takten")
    print(*regKey)
    print("")

def shiftKeyOutput(x):
    # Register regKey x mal shiften und Output verwerfen
    tmpRegOutput = ""
    for i in range(x):
        tmpBit0 = 0
        tmpRegOutput = str(regKey[len(regKey) - 1]) + tmpRegOutput
        for t in TAPPED:
            tmpBit0 += int(regKey[t])
   
        regKey.insert(0, tmpBit0 % 2)
        regKey.pop()
    return tmpRegOutput

def initKeyReg():
    # Key und IV Register initialisieren
    # Beide Register Bitweise 0 setzen
    for i in range(0, 128):
        regKey.append(0)

    for i in range(0, 64):
        regIV.append(0)
    
    print("LSFR bitweise 0 setzten")
    print(*regKey)
    print("")

    # Key und IV ins Register laden
    # (durch das [2:] wird beim Binärwert erst der 2. Index berücksichtigt, somit fällt das 0b weg)
    tmpStart = 64 - len(bin(IV)) + 2
    for binDigit in bin(IV)[2:]:
        regIV[tmpStart] = binDigit
        tmpStart += 1

    tmpStart = 128 - len(bin(KEY)) + 2
    for binDigit in bin(KEY)[2:]:
        regKey[tmpStart] = binDigit
        tmpStart += 1
    
    print("Key ins LSFR laden")
    print(*regKey)
    print("")

    # Key Register 128 mal shiften und Output verwerfen
    shiftKeyReg(128)

    # IV und Key XOR verknüpfen und das Register weitere 128 mal takten
    for i in range(0, 64):
        regKey[i] = int(regKey[i]) ^ int(regIV[i])
    print("Der 64 Bit IV wird ebenfalls ins Register geladen und XOR verknüpft")
    print("IV=", *regIV)
    print(*regKey)
    print("")
    
    shiftKeyReg(128)



print("")
print("Übung 2c - 128 Bit Schieberegister")
print("==================================")
print("")

if __name__ == "__main__":
    main(sys.argv[1:])

print("128 Bit LSFR erstellen")
print("")

initKeyReg()

print("Alle bisherigen Ausgangsbits des LSFR wurden verworfen")
print("Die ersten", SHIFTS, "Ausgabebits:")
print(shiftKeyOutput(SHIFTS))
print("")
